package com.soft.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soft.entity.AdvertEntity;;

@Repository
public interface AdvertDao extends JpaRepository<AdvertEntity, Long> {
}
