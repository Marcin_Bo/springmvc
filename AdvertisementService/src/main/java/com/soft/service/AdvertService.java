package com.soft.service;

import java.util.List;

import com.soft.entity.AdvertEntity;

public interface AdvertService {

	public void create(AdvertEntity entity);

	public List<AdvertEntity> getAll();

	public void delete(Long id);
}
