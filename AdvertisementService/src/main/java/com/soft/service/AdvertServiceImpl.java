package com.soft.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.dao.AdvertDao;
import com.soft.entity.AdvertEntity;

@Service
public class AdvertServiceImpl implements AdvertService {

	@Autowired
	private AdvertDao advertDAO;

	@Transactional
	public void create(AdvertEntity entity) {
		advertDAO.save(entity);
	}

	@Transactional
	public List<AdvertEntity> getAll() {
		return advertDAO.findAll();
	}

	@Transactional
	public void delete(Long id) {
		advertDAO.delete(id);
	}
}
